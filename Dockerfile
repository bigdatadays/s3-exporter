ARG SPARK_IMAGE=gcr.io/spark-operator/spark:v2.4.4

FROM ${SPARK_IMAGE}

RUN mkdir -p /usr/lib/spark-data/spark-jars

COPY target/beam-job-1.0-SNAPSHOT.jar /usr/lib/spark-data/spark-jars/

ENTRYPOINT [ "/opt/entrypoint.sh" ]
