package bigdatadays.beam.options;

import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;

public interface BeamJobOptions extends PipelineOptions {

    @Description("The path to AVRO schema file")
    String getAvroSchemaPath();

    void setAvroSchemaPath(String value);

    @Description("The path to read input files")
    @Default.String("")
    String getInputPath();

    void setInputPath(String value);

    @Description("ClickHouse subprotocol")
    @Default.String("clickhouse")
    String getSubprotocol();

    void setSubprotocol(String value);

    @Description("ClickHouse host")
    String getHost();

    void setHost(String value);

    @Description("ClickHouse port")
    @Default.String("8123")
    String getPort();

    void setPort(String value);

    @Description("Username to connect to ClickHouse database")
    @Default.String("default")
    String getUser();

    void setUser(String value);

    @Description("Password used to connect to ClickHouse database")
    @Default.String("")
    String getPassword();

    void setPassword(String value);

    @Description("ClickHouse database")
    @Default.String("default")
    String getDatabase();

    void setDatabase(String value);

    @Description("ClickHouse table")
    String getTable();

    void setTable(String value);

}
